<?php
namespace Polygon\Realtimeco;

use Illuminate\Contracts\Config\Repository as ConfigRepository;

class Realtimeco extends Realtime{

    /**
     * @var \Illuminate\Contracts\Config\Repository
     */
    protected $config;

    /**
     * @param \Illuminate\Contracts\Config\Repository $config
     */
    public function __construct(ConfigRepository $config){
        $this->config = $config;
        parent::__construct(
            $this->config->get('realtimeco.url'),
            $this->config->get('realtimeco.app_key'),
            $this->config->get('realtimeco.app_private_key'),
            $this->config->get('realtimeco.token')
        );
    }
}