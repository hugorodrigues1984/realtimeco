<?php

namespace Polygon\Realtimeco\Facades;

use Illuminate\Support\Facades\Facade;

class RealtimecoFacade extends Facade {

    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor() {
        return 'Realtimeco';
    }

}