<?php namespace Polygon\Realtimeco;

use Illuminate\Support\ServiceProvider;

class RealtimecoServiceProvider extends ServiceProvider {

	/**
	 * Bootstrap the application services.
	 *
	 * @return void
	 */
	public function boot()
	{
        $configPath = __DIR__ . '/../../config/realtimeco.php';
        $this->publishes([$configPath => config_path('realtimeco.php')], 'config');

        $this->app->bind('Realtimeco', function ($app) {
            return new Realtimeco($app['config']);
        });
	}

	/**
	 * Register the application services.
	 *
	 * @return void
	 */
	public function register()
	{
		//
	}

    public function provides() {
        return array("Realtimeco");
    }

}
