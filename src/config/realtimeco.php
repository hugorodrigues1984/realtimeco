<?php

return [
    'url' => env('REALTIME_URL','http://ortc-developers.realtime.co/server/2.1'),
    'app_key' =>  env('REALTIME_APP_KEY'),
    'app_private_key' => env('REALTIME_PRIVATE_KEY'),
    'token' => env('REALTIME_TOKEN'),
];