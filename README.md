[![Latest Stable Version](https://poser.pugx.org/polygon/realtimeco/v/stable)](https://packagist.org/packages/polygon/realtimeco) [![Total Downloads](https://poser.pugx.org/polygon/realtimeco/downloads)](https://packagist.org/packages/polygon/realtimeco) [![Latest Unstable Version](https://poser.pugx.org/polygon/realtimeco/v/unstable)](https://packagist.org/packages/polygon/realtimeco) [![License](https://poser.pugx.org/polygon/realtimeco/license)](https://packagist.org/packages/polygon/realtimeco)

## Installation

Begin by installing this package through Composer. Edit your project's `composer.json` file to require `polygon/realtimeco`.

    "require": {
		"polygon/realtimeco": "dev-master"
	}

Next, update Composer from the Terminal:

    composer update

Once this operation completes, the next step is to add the service provider. Open `config/app.php`, and add a new item to the providers array.

    'Poligon\Realtimeco\RealtimecoServiceProvider',

Now add the alias.
```php
'aliases' => array(
    ...
	'Realtimeco' => 'Polygon\Realtimeco\Facades\RealtimecoFacade',
)
```

From the terminal run:

    php artisan vendor:publish

a configuration file should be added to config/ folder, called realtimeco.php.

Set this variables in your .env file

REALTIME_URL=http://ortc-developers.realtime.co/server/2.1
REALTIME_APP_KEY=YOUR_APPLICATION_KEY
REALTIME_PRIVATE_KEY=YOUR_APPLICATION_PRIVATE_KEY
REALTIME_TOKEN=dummyToken

You can get this info after creating an account at:
http://www.realtime.co/

## Usage example
        
        $channel = 'my_channel';
        $permission = 'w';
        $private = 0;
        $ttl = 180000;
        $message = ['status' => 'ok', 'msg' => 'Some message'];
        $realtime = new Realtimeco(App::call('config'));
        $realtime->auth([$channel => $permission],$private,$ttl);
        $realtime->send($channel, json_encode($message));